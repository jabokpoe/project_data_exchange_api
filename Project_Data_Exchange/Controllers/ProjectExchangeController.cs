﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Project_Data_Exchange.Models;
using net.sf.mpxj;
using net.sf.mpxj.reader;
using net.sf.mpxj.MpxjUtilities;
using Newtonsoft.Json;
using System.IO;
using System.Web;

namespace Project_Data_Exchange.Controllers
{
    public class ProjectExchangeController : ApiController
    {
        
        private Models.Task InitializeTask(bool excludeParentTask, net.sf.mpxj.Task task)
        {
            if (excludeParentTask)
            {
                return new Models.Task
                {
                    ID = task.ID.intValue(),
                    //Duration = task.Duration.Duration,
                    //UniqueID = task.UniqueID.intValue(),
                    Start = task.Start.ToDateTime().Date,
                    Finish = task.Finish.ToDateTime().Date,
                    Milestone = task.Milestone,
                    Name = task.Name.Trim(),
                    Notes = task.Notes.Trim(),
                    ChildTasksCount = task.ChildTasks.size(),
                    ParentTaskId = task.ParentTask == null ? -1 : task.ParentTask.ID.intValue(),
                    PercentageComplete = task.PercentageComplete.intValue(),
                    PercentageWorkComplete = task.PercentageWorkComplete.intValue()
                };
            }

            return new Models.Task
            {
                ID = task.ID.intValue(),
                //Duration = task.Duration.Duration,
                //UniqueID = task.UniqueID.intValue(),
                Start = task.Start.ToDateTime().Date,
                Finish = task.Finish.ToDateTime().Date,
                Milestone = task.Milestone,
                Name = task.Name.Trim(),
                Notes = task.Notes.Trim(),
                //ChildTasks = task.ChildTasks.isEmpty() ? new List<Models.Task>() : Initializes(task.ChildTasks),
                ChildTasksCount = task.ChildTasks.size(),
                ParentTaskId = task.ParentTask == null ? -1 : task.ParentTask.ID.intValue(),
                PercentageComplete = task.PercentageComplete.intValue(),
                PercentageWorkComplete = task.PercentageWorkComplete.intValue(),
                //ParentTask = task.ParentTask == null ? null : InitializeTask(true, task.ParentTask)
            };
        }

        private List<Models.Task> Initializes(java.util.List tasks)
        {
            List<Models.Task> childTasks = new List<Models.Task>();

            foreach (net.sf.mpxj.Task task in tasks.ToIEnumerable())
            {
                Models.Task _task = InitializeTask(true, task);
                childTasks.Add(_task);
            }
            return childTasks;
        }
        
        public HttpResponseMessage PostProcessProjectFile()
        {
            if(!Request.Content.IsMimeMultipartContent())
            {
                return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
            }


            try
            {
                var file = HttpContext.Current.Request.Files[0];
                string fileNameGuid = Guid.NewGuid().ToString();
                string filePath = HttpContext.Current.Server.MapPath($"~/{fileNameGuid}_{file.FileName}");
                file.SaveAs(filePath);

                ProjectReader reader = ProjectReaderUtility.getProjectReader(filePath);
                ProjectFile mpp = reader.read(filePath);
                TaskContainer tasks = mpp.Tasks;
                List<Models.Task> allProjectTasks = new List<Models.Task>();

                foreach (net.sf.mpxj.Task task in tasks)
                {
                    Models.Task projectTask = InitializeTask(false, task);
                    allProjectTasks.Add(projectTask);
                }

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                return Request.CreateResponse(HttpStatusCode.OK, allProjectTasks);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, new { errors =  e.Message });
            }

        }
    }
}
