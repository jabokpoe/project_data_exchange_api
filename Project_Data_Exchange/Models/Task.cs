﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using net.sf.mpxj;

namespace Project_Data_Exchange.Models
{
    public class Task
    {
        public Task()
        {
             
        }

        public int ID { get; set; }

        //public double Duration { get; set; }

        //public int UniqueID { get; set; }

        public DateTime Start { get; set; }

        public DateTime Finish { get; set; }

        public bool Milestone { get; set; }

        public String Name { get; set; }

        public String Notes { get; set; }

        //public List<Task> ChildTasks { get; set; }

        public int ChildTasksCount { get; set; }

        //public Task ParentTask { get; set; }

        public int ParentTaskId { get; set; }

        public int PercentageComplete { get; set; }

        public int PercentageWorkComplete { get; set; }

       
        
    }
}